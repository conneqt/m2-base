<?php
/**
 * Created by PhpStorm.
 * User: jeroen
 * Date: 16-10-17
 * Time: 21:41
 */

namespace Conneqt\Base\Api;

interface TransactionRepositoryInterface
{
    /**
     * @param bool $includePulled
     * @param bool $updatePulled
     * @return \Conneqt\Base\Api\Data\TransactionSearchResultInterface
     */
    public function getList($includePulled, $updatePulled);

    /**
     * @param int $transactionId
     * @return \Conneqt\Base\Api\Data\TransactionInterface
     */
    public function setPulled($transactionId);
}
<?php

namespace Conneqt\Base\Api\Data;

interface BundledOptionSelectionInterface
{
    /**
     * @param $sku string
     * @return \Conneqt\Base\Api\Data\BundledOptionSelectionInterface
     */
    public function setSku($sku);

    /**
     * @return string
     */
    public function getSku();
}
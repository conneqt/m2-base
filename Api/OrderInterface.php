<?php

namespace Conneqt\Base\Api;

interface OrderInterface
{
    /**
     * @param \Magento\Sales\Api\Data\OrderInterface $order
     * @return \Magento\Sales\Api\Data\OrderInterface
     */
    public function placeOrder($order);
}
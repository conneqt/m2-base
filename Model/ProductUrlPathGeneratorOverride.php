<?php

namespace Conneqt\Base\Model;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\CatalogUrlRewrite\Model\CategoryUrlPathGenerator;
use Magento\CatalogUrlRewrite\Model\ProductUrlPathGenerator;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\StoreManagerInterface;

class ProductUrlPathGeneratorOverride extends ProductUrlPathGenerator
{
    /**
     * Prepare URL Key with stored product data (fallback for "Use Default Value" logic)
     *
     * @param Product $product
     * @return string
     */
    protected function prepareProductDefaultUrlKey(Product $product)
    {
        $storedProduct = $this->productRepository->getById($product->getId());
        $storedUrlKey = $storedProduct->getUrlKey();
        return $storedUrlKey ?: $product->formatUrlKey($this->useSkuUrlKey() ? $storedProduct->getSku() : $storedProduct->getName());
    }

    /**
     * Prepare url key for product
     *
     * @param Product $product
     * @return string
     */
    protected function prepareProductUrlKey(Product $product)
    {
        $urlKey = (string)$product->getUrlKey();
        $urlKey = trim(strtolower($urlKey));

        return $product->formatUrlKey($urlKey ?: ($this->useSkuUrlKey() ? $product->getSku() : $product->getName()));
    }

    protected function useSkuUrlKey(): bool
    {
        return (bool)$this->scopeConfig->getValue('conneqt_base/product_settings/sku_url_key');
    }
}

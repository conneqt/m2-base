<?php

namespace Conneqt\Base\Model;

class BundledOptionSelection implements \Conneqt\Base\Api\Data\BundledOptionSelectionInterface
{
    /** @var string */
    protected $sku;

    /**
     * @param $sku string
     * @return \Conneqt\Base\Api\Data\BundledOptionSelectionInterface
     */
    public function setSku($sku)
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * @return string
     */
    public function getSku()
    {
        return $this->sku;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: jeroen
 * Date: 16-10-17
 * Time: 20:02
 */

namespace Conneqt\Base\Observer;

class OrderObserver extends BaseObserver implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $transaction = $this->_transactionHelper->addTransaction(
            \Magento\Sales\Model\Order::ENTITY,
            $observer->getOrder()->getId()
        );
    }
}
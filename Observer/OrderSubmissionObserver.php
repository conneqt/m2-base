<?php

namespace Conneqt\Base\Observer;

class OrderSubmissionObserver implements \Magento\Framework\Event\ObserverInterface
{
    /** @var \Magento\Framework\Webapi\Request */
    protected $request;

    public function __construct(\Magento\Framework\Webapi\Request $request)
    {
        $this->request = $request;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Magento\Sales\Model\Order $order */
        $order = $observer->getOrder();

        if (strpos($this->request->getPathInfo(), 'conneqt_orders') !== false) {
            $order->setCanSendNewEmailFlag(false);
        }
    }
}
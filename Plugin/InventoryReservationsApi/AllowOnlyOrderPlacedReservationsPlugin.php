<?php

namespace Conneqt\Base\Plugin\InventoryReservationsApi;

use Closure;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\InventoryReservationsApi\Model\AppendReservationsInterface;
use Magento\InventoryReservationsApi\Model\ReservationInterface;

/**
 * Prevent append reservation if it's not an "order_placed" eventType
 */
class AllowOnlyOrderPlacedReservationsPlugin
{
    /**
     * @var SerializerInterface
     */
    private $serializer;
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @param SerializerInterface $serializer
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        SerializerInterface $serializer
    ) {
        $this->serializer = $serializer;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @param AppendReservationsInterface $subject
     * @param Closure $proceed
     * @param ReservationInterface[] $reservations
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundExecute(AppendReservationsInterface $subject, Closure $proceed, array $reservations)
    {
        if ($this->isEnabled()) {
            $reservationToAppend = [];
            foreach ($reservations as $reservation) {

                $metaData = $this->serializer->unserialize($reservation->getMetadata());
                if (is_array($metaData) && $metaData['event_type'] == 'order_placed') {
                    $reservationToAppend[] = $reservation;
                }
            }

            if (!empty($reservationToAppend)) {
                $proceed($reservationToAppend);
            }
        } else {
            $proceed($reservations);
        }
    }

    private function isEnabled(): bool
    {
        return ($this->scopeConfig->getValue('conneqt_base/settings/remove_reservations_experimental') === '1');
    }
}
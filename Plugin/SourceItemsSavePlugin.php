<?php

namespace Conneqt\Base\Plugin;

class SourceItemsSavePlugin
{
    private $scopeConfig;
    private $sourceRepository;
    private $resourceConnection;
    private $deleteReservationsBySkus;

    public function __construct(
        \Magento\InventorySales\Model\ResourceModel\DeleteReservationsBySkus $deleteReservationsBySkus,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->deleteReservationsBySkus = $deleteReservationsBySkus;
    }

    public function aroundExecute($subject, callable $proceed, $sourceItems)
    {
        $proceed($sourceItems);

        if ($this->isEnabled() && !empty($sourceItems)) {
            $skus = [];
            foreach ($sourceItems as $sourceItem) {
                $skus[] = $sourceItem->getSku();
            }

            $this->deleteReservationsBySkus->execute($skus);
        }
    }

    private function isEnabled(): bool
    {
        return
            $this->scopeConfig->getValue('conneqt_base/settings/remove_reservations_experimental') === '1' ||
            $this->scopeConfig->getValue('conneqt_base/settings/remove_reservations') === '1';
    }
}
